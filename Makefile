DEBUG=tru
CC=g++
WXCONFIG=wx-config
ifeq "$(DEBUG)" "true"
DEBUGOPTS=-g -O0
else
DEBUGOPTS=-O3 -DNDEBUG
endif
CXXFLAGS=-Wall -Wextra -pedantic -Werror -std=c++20 $(DEBUGOPTS) $(shell $(WXCONFIG) --cxxflags) -fdiagnostics-color=always -DSPDLOG_FMT_EXTERNAL=ON
export LIBSCARLETT_STATIC=false
ifeq "$(LIBSCARLETT_STATIC)" "true"
libscarlett=libscarlett.a
libscarlett_link=dist/libscarlett.a -lasound
else
libscarlett=libscarlett.so
libscarlett_link=-Ldist -lscarlett
endif
libscarlett_dir=libscarlett/dist
libscarlett_include=libscarlett/include
sources=$(wildcard src/*.cpp)
objects=$(sources:src/%.cpp=obj/%.o)
headers=$(wildcard src/*.hpp)
precompiledheader=src/stdafx.hpp.gch
executable=dist/mixer
resourcesrc=$(shell find resources/ -type f)
resourcedest=$(resourcesrc:resources/%=dist/%)
headerdependencies=$(objects:obj/%.o=obj/%.d)

# "|" signals an order-only prerequisite: the prereq must exist, but
# it won't cause the target to be recreated.
$(executable): $(precompiledheader) $(objects) dist/$(libscarlett) Makefile | obj dist $(resourcedest)
	@echo Making $@
	@$(CC) $(CXXFLAGS) -o $@ \
		$(objects) \
		-lfmt \
		$(shell $(WXCONFIG) --libs) \
		$(libscarlett_link) -lstdc++fs
	@echo "Made  " $@

$(resourcedest) : dist/% : resources/% | dist
	mkdir -p $(dir $@);
	cp -r $< $@;

obj dist:
	if [ ! -e $@ ]; then mkdir $@; fi;

-include $(headerdependencies)

reformat:
	for file in $(sources) $(headers); do \
		clang-format -i -style="$$(<style)" "$$file"; \
	done;

# Static pattern rules. Take each object in the targets, create a "base"
# with it specified by % (removing the literal part '.o'), then
# use that base to generate the prerequisite. This generates
# several rules.  For instance,  main.o : "main".o : "main".cpp
# then refer to the left-most prerequisite with $<, and the target
# with $@ like usual.
$(objects) : obj/%.o : src/%.cpp $(headers) $(precompiledheader) Makefile | obj
	@echo Making $@
	@$(CC) -c $(CXXFLAGS) \
		$(shell $(WXCONFIG) --cppflags) \
		-I $(libscarlett_include) -Isrc/spdlog/include \
		$< -o $@
	@echo "Made  " $@

$(precompiledheader) : src/stdafx.hpp src/logging.hpp Makefile $(wildcard $(libscarlett_include)/scarlett/*) | obj
# Making wxwidgets has LOTS of warning output. Swallow it for a sane compilation.
	@echo Making $@ AND SUPRESSING OUTPUT
	@$(CC) $(CXXFLAGS) \
		$(shell $(WXCONFIG) --cppflags) \
		-Wno-error=deprecated-copy -Wno-error -Wno-error=ignored-qualifiers \
		-I $(libscarlett_include) -Isrc/spdlog/include \
		$< -o $@
	@echo "Made  " $@

dist/$(libscarlett) : $(libscarlett_dir)/$(libscarlett)
	cp $< $@;

$(libscarlett_dir)/$(libscarlett) : FORCE
	cd libscarlett && $(MAKE);

FORCE :

clean :
	rm -rf obj dist $(precompiledheader)
	cd libscarlett; $(MAKE) clean

echo :
	@echo sources $(sources)
	@echo objects $(objects)
	@echo headers $(headers) src/stdafx.hpp
	@echo executable $(executable)
	@echo resourcesrc $(resourcesrc)
	@echo resourcedest $(resourcedest)

.PHONY : echo clean reformat
