#pragma once

#include <scarlett/mixer_wrapper.hpp>

struct mix_manager : public wxPanel
{
    mix_manager(wxWindow *, wxApp *, scarlett::mixer_wrapper &);
    void select_output(std::string);
    bool Layout(void) override;

private:
    wxApp * app {nullptr};
    scarlett::mixer_wrapper & wrapper;
    std::string current_output;
    std::vector<struct mix_panel *> mix_members;
    wxButton * plus_btn {nullptr};
    bool updating_panels {false};

    void button_clicked(void);
    void requeted_add_input(wxCommandEvent const &);
    void output_changed(std::string const &, std::string const &, bool);
    void remove_input(std::string const &);
    void add_input_panel(std::string const &);
    void requested_add_input(wxCommandEvent const &);
    void mix_volume_changed(std::string const &, std::string const &, int);
    void update_panels(void);
    void update_panels_impl(void);
};
