#include "stdafx.hpp"
#include "logging.hpp"

#include "spdlog/async.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_sinks.h"

unsigned logger::get_uptime(void) const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();
}

std::shared_ptr<spdlog::logger> logger::get_logger(void) const
{
    return logger;
}

struct initer
{
    initer(void) { spdlog::init_thread_pool(8192, 1); }
};

void init_thread_pool(void)
{
    static initer i;
}

std::string const format = "%v";

struct cout_sink_initer
{
    cout_sink_initer(void)
    {
        sink = std::make_shared<spdlog::sinks::stdout_sink_mt>();
        sink->set_pattern(format);
    }
    std::shared_ptr<spdlog::sinks::sink> sink;
};

std::shared_ptr<spdlog::sinks::sink> get_cout_sink(void)
{
    static cout_sink_initer i;
    return i.sink;
}

/* Logging configuration here */
struct main_logger : public logger
{
    main_logger(void)
    {
        init_thread_pool();
        std::shared_ptr<spdlog::sinks::sink> file_sink {std::make_shared<spdlog::sinks::basic_file_sink_mt>("log.log")};
        file_sink->set_pattern(format);
        auto sinks = {file_sink, get_cout_sink()};
        logger = std::make_shared<spdlog::async_logger>("main",
            sinks, spdlog::thread_pool(), spdlog::async_overflow_policy::block);
        logger->set_level(spdlog::level::debug);
    }

private:
};

logger const & get_main_logger(void)
{
    static main_logger m;
    return m;
}

std::string prepare_file_name(char const * file)
{
    std::string name {file};
    auto index {name.find_last_of('/')};
    name = name.substr(index + 1);
    index = name.find_last_of('.');
    name = name.substr(0, index);
    return name;
}

thread_local std::string thread_name;

void set_thread_name(std::string const & name)
{
    if (thread_name.size())
    {
        get_main_logger().get_logger()->error("Attempt to set thread name to {} after it was already set to {}.",
            name, thread_name);
        return;
    }
    thread_name = name;
}

std::string get_thread_string(void)
{
    if (!thread_name.size())
    {
        std::hash<std::thread::id> hasher;
        return "t:" + std::to_string(hasher(std::this_thread::get_id()));
    }
    return "t:" + thread_name;
}
