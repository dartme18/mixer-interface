#pragma once

/*
 * The left bar inside the main frame. Manages several
 * output_panels.
 */
struct output_manager : public wxPanel
{
    using output_selected_handler = std::function<void(std::string const &)>;
    output_manager(wxWindow *, wxApp *, scarlett::imixer &);
    void register_output_handler(output_selected_handler const &);

private:
    scarlett::imixer & mixer;
    wxApp * app {nullptr};
    struct output_panel * selected_output {nullptr};
    std::vector<output_panel *> outputs;
    std::vector<output_selected_handler> output_selected;
    wxButton * btn_add_usb {nullptr};

    void output_clicked(output_panel *);
    void add_output(output_panel *);
    void start_add_usb_output(void);
    void add_usb_output(wxCommandEvent const &);
};
