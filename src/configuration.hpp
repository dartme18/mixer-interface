#pragma once

#include <scarlett/ienum_control.hpp>
#include <scarlett/ivolume_control.hpp>

struct configuration
{
    /* Does not read a configuration file. */
    configuration(void) = default;
    configuration(std::string const & config_file);
    /* If the default config file doesn't exist, we attempt to create it. */
    static configuration read_default_config_file(void);
    static std::string const default_config_file;
    void apply_startup_settings(scarlett::idevice &) const;
    void save_configuration(scarlett::idevice const &) const;

private:
    std::string config_file;

    using setting_t = std::pair<std::string, long>;
    using settings_t = std::vector<setting_t>;

    settings_t get_settings(std::string const &) const;
    void apply_settings(scarlett::idevice & dev, settings_t const &) const;
    bool apply_enum_setting(setting_t const &, std::vector<scarlett::ienum_ptr> const &) const;
    bool apply_volume_setting(setting_t const &, std::vector<scarlett::ivolume_ptr> const &) const;
    std::filesystem::path get_device_config_file(std::string const &, bool = false) const;
    std::filesystem::path resolve_device_config_file_location(std::string const &) const;
};
