#include "stdafx.hpp"

#include <scarlett/scarlett.hpp>
#include "logging.hpp"

#include "main_frame.hpp"
#include "output_manager.hpp"
#include "mix_manager.hpp"

main_frame::main_frame(scarlett::imixer & mixer, scarlett::mixer_wrapper & wrapper, wxApp * app)
    : wxFrame {nullptr, wxID_ANY, "Scarlett Mixer Interface"}
    , main_panel {new wxPanel {this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL}}
    , mixer {mixer}
    , wrapper {wrapper}
    , outputs {new output_manager {main_panel, app, mixer}}
    , mixes {new mix_manager {main_panel, app, wrapper}}
{
    SetMinSize({400, 600});
    auto main_sizer {new wxBoxSizer {wxHORIZONTAL}};
    main_sizer->Add(outputs, wxSizerFlags().Proportion(1).Expand());
    main_sizer->Add(mixes, wxSizerFlags().Proportion(3).Expand());
    main_panel->SetSizer(main_sizer);
    outputs->register_output_handler([this](auto const & out) {
        mixes->select_output(out);
    });
    mixes->select_output(wrapper.get_outputs()[0]);
}
