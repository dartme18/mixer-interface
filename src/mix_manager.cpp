#include "stdafx.hpp"
#include "logging.hpp"

#include "mix_manager.hpp"
#include "mix_panel.hpp"
#include "util.hpp"

mix_manager::mix_manager(wxWindow * parent, wxApp * app, scarlett::mixer_wrapper & wrapper)
    : wxPanel {parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL | wxBORDER_SUNKEN}
    , app {app}
    , wrapper {wrapper}
    , plus_btn {new wxButton {this, wxID_ANY, "+"}}
{
    plus_btn->SetToolTip("Add Input to Mix");
    auto font {plus_btn->GetFont()};
    font.SetPointSize(font.GetPointSize() * 4);
    plus_btn->SetFont(font);
    plus_btn->Bind(wxEVT_BUTTON, [this](auto const &) { button_clicked(); });
    //SetBackgroundColour(*wxYELLOW);
    wrapper.add_mix_output_listener([this](auto const & out, auto const & in, bool added) {
        /* For thread safety */
        auto top_window {this->app->GetTopWindow()};
        if (!top_window)
            return;
        /* Copy everything into the lambda! */
        top_window->GetEventHandler()->CallAfter([this, out, in, added]() {
            output_changed(out, in, added);
        });
    });
    wrapper.add_mix_volume_listener([this](auto const & out, auto const & in, int new_vol) {
        callback(this->app, [this, out, in, new_vol] { mix_volume_changed(out, in, new_vol); });
    });
    SetAutoLayout(true);
}

void mix_manager::remove_input(std::string const & input)
{
    auto spot {std::find_if(mix_members.begin(), mix_members.end(), [&input](auto const & temp_input) {
        return temp_input->get_input_name() == input;
    })};
    if (spot == mix_members.end())
    {
        ERROR("Couldn't find an input we're supposed to remove! Continuing...");
        return;
    }
    (*spot)->Destroy();
    mix_members.erase(spot);
    update_panels();
    Refresh();
    Layout();
}

void mix_manager::mix_volume_changed(std::string const & out, std::string const & in, int new_vol)
{
    if (out != current_output)
        return;
    auto spot {std::find_if(mix_members.begin(), mix_members.end(), [&in](auto const & temp_input) {
        return temp_input->get_input_name() == in;
    })};
    if (spot == mix_members.end())
    {
        ERROR("Mix volume changed, but output, {}, doesn't have input, {}. Continuing.", out, in);
        return;
    }
    if ((*spot)->has_volume() != (new_vol != scarlett::mixer_wrapper::no_volume))
        update_panels();
    else
        (*spot)->set_volume(new_vol);
}

void mix_manager::output_changed(std::string const & out, std::string const & in, bool added)
{
    if (out != current_output)
        return;
    if (added)
        add_input_panel(in);
    else
        remove_input(in);
}

void mix_manager::update_panels(void)
{
    callback(app, [this] { update_panels_impl(); });
}

void mix_manager::update_panels_impl(void)
{
    if (updating_panels)
        return;
    updating_panels = true;
    auto inputs {wrapper.get_inputs(current_output)};
    for (auto const & input : inputs)
    {
        auto spot {std::find_if(mix_members.begin(), mix_members.end(), [&input](auto const & temp_input) {
            return temp_input->get_input_name() == input.input;
        })};
        if (spot == mix_members.end())
        {
            INFO("Couldn't find input, {}, while updating. Continuing...", input.input);
            add_input_panel(input.input);
        }
        else if ((*spot)->has_volume() != (input.vol != scarlett::mixer_wrapper::no_volume))
        {
            INFO("Updating panel for input, {}.", input.input);
            remove_input(input.input);
            add_input_panel(input.input);
        }
    }
    updating_panels = false;
}

void mix_manager::add_input_panel(std::string const & input)
{
    mix_panel * new_panel {nullptr};
    auto current_volume {wrapper.get_mix_volume(current_output, input)};
    if (current_volume == scarlett::mixer_wrapper::no_volume)
    {
        new_panel = new mix_panel {this, input};
    }
    else
    {
        auto max {wrapper.get_mix_input_max_db(current_output, input)};
        auto min {wrapper.get_mix_input_min_db(current_output, input)};
        new_panel = new mix_panel {this, min, max, current_volume, input};
        new_panel->register_volume_change([this, new_panel](int new_vol) {
            wrapper.set_mix_volume(current_output, new_panel->get_input_name(), new_vol);
        });
    }
    new_panel->register_remove_requested([this, new_panel] {
        wrapper.remove_input_from_output(current_output, new_panel->get_input_name());
    });
    mix_members.push_back(new_panel);
    update_panels();
    Refresh();
    Layout();
}

void mix_manager::requested_add_input(wxCommandEvent const & evt)
{
    wrapper.add_input_to_output(current_output, wrapper.get_mix_inputs()[evt.GetId()]);
}

void mix_manager::button_clicked(void)
{
    wxMenu menu;
    auto inputs {wrapper.get_mix_inputs()};
    auto existing_inputs {wrapper.get_inputs(current_output)};
    bool added_one {false};
    for (unsigned i {0}; i < inputs.size(); ++i)
    {
        auto const & input {inputs[i]};
        auto spot {std::find_if(existing_inputs.begin(), existing_inputs.end(), [&input](auto const & a) {
            return a.input == input;
        })};
        if (spot != existing_inputs.end())
            continue;
        menu.Append(i, input);
        added_one = true;
    }
    if (!added_one)
        return;
    menu.Bind(wxEVT_COMMAND_MENU_SELECTED, [this](auto const & evt) { requested_add_input(evt); });
    plus_btn->PopupMenu(&menu);
}

bool mix_manager::Layout(void)
{
    auto my_size {GetSize()};
    plus_btn->SetSize(0, 0, 70, my_size.GetHeight() / 2);
    int left {plus_btn->GetSize().GetWidth()};
    int top {0};
    int const def_width {100};
    for (auto const & pnl : mix_members)
    {
        if (left + def_width > my_size.GetWidth())
        {
            if (top > 0)
            {
                pnl->Hide();
                continue;
                /* We don't draw a third row just now. */
            }
            top = my_size.GetHeight() / 2;
            left = 0;
        }
        pnl->Show();
        pnl->SetSize(left, top, def_width, my_size.GetHeight() / 2);
        left += def_width;
        DEBUG("Setting panel to {}, {}, {}, {}.", left, top, def_width, my_size.GetHeight() / 2);
    }
    return true;
}

void mix_manager::select_output(std::string out)
{
    if (current_output == out)
        return;
    current_output = out;
    for (auto panel : mix_members)
        panel->Destroy();
    mix_members.clear();
    update_panels();
}
