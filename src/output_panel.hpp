#pragma once

#include <scarlett/ivolume_control.hpp>

/* Shows a single output's name and volume.
 * Managed by output_manager
 */
struct output_panel : public wxPanel
{
    using click_handler = std::function<void(std::string const &)>;
    output_panel(wxWindow *, scarlett::ienum_control const &, std::string const & display_name, wxApp *);
    output_panel(wxWindow *, scarlett::ienum_control const &, scarlett::ivolume_control &, wxApp *);
    ~output_panel(void);
    void set_selected(bool);
    void register_click_handler(click_handler const &);
    std::string get_output_name(void) const;

private:
    std::vector<click_handler> clicked;
    std::string const output;
    scarlett::ivolume_control * const vol {nullptr};
    wxSlider * const slider {nullptr};
    wxStaticText * const volume {nullptr};
    wxApp * const app {nullptr};

    wxSizer * init(std::string const &);
    void fire_clicked(void);
    void handle_volume_changed_external(scarlett::ivolume_control const &);
    void handle_slider_changed(wxScrollEvent &);
};
