#pragma once
#include "util.hpp"

/*
 * A single input of the current output's mix. Shows the input
 * choice and its volume.
 * Managed by mix_manager.
 */
struct mix_panel : public wxWindow
{
    mix_panel(wxWindow * parent, std::string const & name);
    mix_panel(wxWindow * parent, int min_vol, int max_vol, int current_volume, std::string const & name);
    /* Given the new input assignment, return the volume for that input. */
    using volume_change_handler_t = std::function<void(int)>;
    void register_volume_change(volume_change_handler_t const &);
    void register_remove_requested(void_func_t const &);
    std::string const & get_input_name(void) const;
    void set_volume(int);
    bool has_volume(void) const;
    bool Layout(void) override;
    void paint(wxPaintEvent &);

private:
    volume_change_handler_t volume_change_handler;
    void_func_t remove_me;
    std::string const input_name;
    wxSlider * volume {};
    wxButton * remove {};
    wxStaticText * input_txt {};
    wxStaticText * vol_txt {};

    void init(void);
    void center(wxWindow *) const;
    void fire_changed(void) const;
    void user_changed_volume(int);
};
