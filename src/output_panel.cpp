#include "stdafx.hpp"

#include "logging.hpp"
#include <scarlett/scarlett.hpp>
#include "output_panel.hpp"
#include "util.hpp"

output_panel::output_panel(wxWindow * parent, scarlett::ienum_control const & out, std::string const & display_name, wxApp *)
    : wxPanel {parent}
    , output {out.get_name()}
    , app {app}
{
    init(display_name);
}

output_panel::output_panel(wxWindow * parent, scarlett::ienum_control const & out,
    scarlett::ivolume_control & vol, wxApp * app)
    : wxPanel {parent}
    , output {out.get_name()}
    , vol {&vol}
    , slider {new wxSlider {this, wxID_ANY, static_cast<int>(vol.get_db()), static_cast<int>(vol.get_min_db()), static_cast<int>(vol.get_max_db()), wxDefaultPosition, wxDefaultSize, wxBORDER_RAISED}}
    , volume {new wxStaticText {this, -1, "not inited"}}
    , app {app}
{
    auto name {output};
    name.erase(0, 7);
    auto temp_sizer {init(name)};
    temp_sizer->Add(volume, wxSizerFlags().Right());
    volume->Bind(wxEVT_LEFT_UP, [this](auto const &) { fire_clicked(); });
    //volume->SetBackgroundColour(*wxBLUE);
    auto func {[this](auto & a) mutable {
        handle_slider_changed(a);
    }};
    slider->Bind(wxEVT_SCROLL_THUMBTRACK, func);
    slider->Bind(wxEVT_SCROLL_CHANGED, func);
    slider->Bind(wxEVT_LEFT_UP, [this](auto const &) { fire_clicked(); });
    temp_sizer->Add(slider, wxSizerFlags().Expand());
    vol.register_volume_changed(this, [this](auto const & vol) { handle_volume_changed_external(vol); });
    handle_volume_changed_external(vol);
}

output_panel::~output_panel(void)
{
    if (vol)
        vol->unregister_all(this);
}

void output_panel::handle_slider_changed(wxScrollEvent & evt)
{
    assert(vol != nullptr);
    vol->set_db(evt.GetPosition());
    fire_clicked();
}

void output_panel::handle_volume_changed_external(scarlett::ivolume_control const & vol)
{
    auto top_window {app->GetTopWindow()};
    if (!top_window)
    {
        ERROR("No top window found!?");
        return;
    }
    auto new_vol {vol.get_db()};
    top_window->GetEventHandler()->CallAfter([this, new_vol] {
        slider->SetValue(static_cast<int>(new_vol));
        volume->SetLabel(volume_to_string(new_vol));
        GetSizer()->Layout();
    });
}

void output_panel::fire_clicked(void)
{
    DEBUG("%s Clicked.", output.c_str());
    for (auto const & h : clicked)
        h(output);
}

wxSizer * output_panel::init(std::string const & name)
{
    auto temp_sizer {new wxBoxSizer {wxVERTICAL}};
    SetSizer(temp_sizer);
    auto temp_txt {new wxStaticText {this, -1, name}};
    temp_sizer->Add(temp_txt);
    Bind(wxEVT_LEFT_UP, [this](auto const &) { fire_clicked(); });
    temp_txt->Bind(wxEVT_LEFT_UP, [this](auto const &) { fire_clicked(); });
    return temp_sizer;
}

void output_panel::register_click_handler(click_handler const & h)
{
    clicked.push_back(h);
}

std::string output_panel::get_output_name(void) const
{
    return output;
}

void output_panel::set_selected(bool selected)
{
    if (selected)
        SetBackgroundColour(*wxBLUE);
    else
        SetBackgroundColour(*wxWHITE);
}
