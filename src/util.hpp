#pragma once

using void_func_t = std::function<void(void)>;

void callback(wxApp *, std::function<void(void)> const &);
std::string volume_to_string(long);
