#pragma once

#include "configuration.hpp"

struct mixer_app : public wxApp
{
    mixer_app(void);
    ~mixer_app(void);
    bool OnInit(void) override;
    bool OnCmdLineParsed(wxCmdLineParser &) override;
    void OnInitCmdLine(wxCmdLineParser &) override;
    int OnExit(void) override;

private:
    scarlett::idevice_ptr dev;
    scarlett::imixer_ptr mixer;
    std::unique_ptr<scarlett::mixer_wrapper> wrapper;
    struct main_frame * frame {nullptr};

    configuration conf;
};
