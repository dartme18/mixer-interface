#pragma once

struct main_frame : public wxFrame
{
    main_frame(scarlett::imixer &, scarlett::mixer_wrapper &, wxApp *);
    /*void OnExit(wxCommandEvent &) {Close(true); }*/
private:
    wxPanel * main_panel {nullptr};
    wxComboBox * combobox {nullptr};
    scarlett::imixer & mixer;
    scarlett::mixer_wrapper & wrapper;
    struct output_manager * outputs {nullptr};
    struct mix_manager * mixes {nullptr};

    void set_sizer(void);
};
