#include "stdafx.hpp"

#include "logging.hpp"
#include <scarlett/scarlett.hpp>
#include "output_manager.hpp"
#include "output_panel.hpp"

std::string convert_usb_output_name(std::string name)
{
    name.erase(0, 13);
    return "USB Output " + name;
}

output_manager::output_manager(wxWindow * parent, wxApp * app, scarlett::imixer & mixer)
    : wxPanel {parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL | wxBORDER_SUNKEN}
    , mixer {mixer}
    , app {app}
{
    auto sizer {new wxBoxSizer {wxVERTICAL}};
    SetSizer(sizer);
    btn_add_usb = new wxButton {this, wxID_ANY, "+"};
    btn_add_usb->SetToolTip("Add USB Output");
    btn_add_usb->Bind(wxEVT_BUTTON, [this](auto const &) { start_add_usb_output(); });
    sizer->Add(btn_add_usb, /*vertical weight*/ 1);
    for (unsigned i {0}; i < mixer.get_output_count(); ++i)
    {
        auto temp_output_panel {new output_panel {this, mixer.get_output_assignment(i),
            mixer.get_output_volume(i), app}};
        add_output(temp_output_panel);
    }
    for (unsigned i {0}; i < std::min(2u, mixer.get_usb_output_count()); ++i)
    {
        auto display_name {convert_usb_output_name(mixer.get_usb_output(i).get_name())};
        auto temp_output_panel {new output_panel {this, mixer.get_usb_output(i), display_name, app}};
        add_output(temp_output_panel);
    }
    SetMinSize({80, 80});
}

void output_manager::start_add_usb_output(void)
{
    wxMenu menu;
    bool added_one {false};
    for (unsigned i {0}; i < mixer.get_usb_output_count(); ++i)
    {
        auto const & usb {mixer.get_usb_output(i)};
        auto spot {std::find_if(outputs.begin(), outputs.end(), [&usb](auto const & a) {
            return a->get_output_name() == usb.get_name();
        })};
        if (spot != outputs.end())
            continue;
        menu.Append(i, convert_usb_output_name(usb.get_name()));
        added_one = true;
    }
    if (!added_one)
        return;
    menu.Bind(wxEVT_COMMAND_MENU_SELECTED, [this](auto const & evt) {
        add_usb_output(evt);
        GetSizer()->Layout();
    });
    /* Blocks until the menu is disposed. */
    btn_add_usb->PopupMenu(&menu);
}

void output_manager::add_usb_output(wxCommandEvent const & evt)
{
    auto const & usb {mixer.get_usb_output(evt.GetId())};
    auto display_name {convert_usb_output_name(usb.get_name())};
    auto temp_output_panel {new output_panel {this, usb, display_name, app}};
    add_output(temp_output_panel);
    output_clicked(temp_output_panel);
}

void output_manager::add_output(output_panel * panel)
{
    GetSizer()->Insert(outputs.size(), panel, /*vertical weight*/ 1, wxEXPAND, 0);
    outputs.push_back(panel);
    panel->register_click_handler([this, panel](auto const &) { output_clicked(panel); });
    if (!selected_output)
    {
        selected_output = panel;
        panel->set_selected(true);
    }
}

void output_manager::register_output_handler(output_selected_handler const & h)
{
    output_selected.push_back(h);
}

void output_manager::output_clicked(output_panel * panel)
{
    DEBUG("output clicked.");
    if (panel == selected_output)
        return;
    if (selected_output)
    {
        DEBUG("deselecting...");
        selected_output->set_selected(false);
    }
    DEBUG("selecting...");
    selected_output = panel;
    selected_output->set_selected(true);
    for (auto const & h : output_selected)
        h(selected_output->get_output_name());
}
