#include "stdafx.hpp"

#include "logging.hpp"

#include <scarlett/scarlett.hpp>
#include "mixer_app.hpp"
#include "main_frame.hpp"

wxIMPLEMENT_APP(mixer_app);

mixer_app::mixer_app(void)
{
}

mixer_app::~mixer_app(void)
{
    DEBUG("Destroying mixer app.");
}

bool mixer_app::OnInit(void)
{
    INFO("INITING 0");
    if (!wxApp::OnInit())
        return false;
    INFO("INITING 1");
    frame = new main_frame {*mixer, *wrapper, this};
    frame->Show(true);
    return true;
}

void mixer_app::OnInitCmdLine(wxCmdLineParser & parser)
{
    wxApp::OnInitCmdLine(parser);
    parser.AddOption("c", "card", "The audio card to use (default is hw:0). "
                                  "For a dummy device, use \"dummy:<dev>\" where <dev> is the device to emulate (e.g. dummy:18i8). "
                                  "Use aplay -l to get a list of the devices on your system. "
                                  "The hw:X,Y comes from this mapping of your hardware -- in this case, "
                                  "X is the card number, while Y is the device number. "
                                  "To cause the dummy device to change volume periodically, append \":a\" to the end of the "
                                  " device name.",
        wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL);
    parser.AddSwitch("d", wxEmptyString, "Generate dummy device code for the sake of testing.");
    parser.AddOption("s", "settings-file", "The default settings file is sought at $HOME/.config/scarlett-interface/scarlett-interface.conf (" + configuration::default_config_file + ").");
    parser.AddSwitch("q", wxEmptyString, "Quit after applying settings from settings file.");
}

bool mixer_app::OnCmdLineParsed(wxCmdLineParser & parser)
{
    INFO("CMD LINE PARSED");
    wxString val;
    std::string device_name {"hw:0"};
    if (parser.Found("c", &val))
        device_name = val;
    if (parser.Found("d"))
    {
        std::cout << scarlett::create_dummy_code(device_name) << '\n';
        return false;
    }
    if (parser.Found("s", &val))
        conf = configuration {std::string {val}};
    else
        conf = configuration::read_default_config_file();
    if (device_name.rfind("dummy:", 0) != std::string::npos)
    {
        device_name.erase(0, 6);
        INFO("Using dummy device \"%s\".", device_name.c_str());
        dev = scarlett::get_dummy_device(device_name);
    }
    else
        dev = scarlett::get_device(device_name);
    dev->register_device_disconnected(this, [this] { frame->Close(true); });
    conf.apply_startup_settings(*dev);
    mixer = scarlett::get_mixer(dev);
    wrapper = std::make_unique<scarlett::mixer_wrapper>(*mixer);
    if (parser.Found("q"))
        return false;
    return wxApp::OnCmdLineParsed(parser);
}

int mixer_app::OnExit(void)
{
    DEBUG("EXITING");
    if (!dev->is_disconnected())
        conf.save_configuration(*dev);
    else
        INFO("Not saving configuration because the device was disconnected.");
    wrapper = nullptr;
    mixer = nullptr;
    return 0;
}
