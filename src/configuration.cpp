#include "stdafx.hpp"
#include "logging.hpp"
#include <scarlett/scarlett.hpp>

#include "configuration.hpp"

std::string const configuration::default_config_file {std::getenv("HOME") + std::string {"/.config/scarlett-interface/scarlett-interface.conf"}};

configuration::configuration(std::string const & file)
    : config_file {file}
{
    DEBUG("Using configuration file {}", file);
}

configuration configuration::read_default_config_file(void)
{
    DEBUG("read default");
    return {default_config_file};
}

bool configuration::apply_enum_setting(setting_t const & setting, std::vector<scarlett::ienum_ptr> const & es) const
{
    auto spot {std::find_if(es.begin(), es.end(), [&setting](auto const & e) {
        return e->get_name() == setting.first;
    })};
    if (spot == es.end())
        return false;
    /* We need to jostle the settings because sometimes the driver says that a certain setting is
     * chosen, but it does not actually take effect until changed. I guess this could be a
     * firmware bug, too? */
    if (0 == setting.second)
    {
        if ((*spot)->get_enum_choices().size() > 0)
            (*spot)->set_selection(1);
    }
    else
        (*spot)->set_selection(0);
    DEBUG("  setting selection {}={}.", setting.first, setting.second);
    (*spot)->set_selection(setting.second);
    return true;
}

bool configuration::apply_volume_setting(setting_t const & setting, std::vector<scarlett::ivolume_ptr> const & vs) const
{
    auto spot {std::find_if(vs.begin(), vs.end(), [&setting](auto const & e) {
        return e->get_name() == setting.first;
    })};
    if (spot == vs.end())
        return false;
    DEBUG("  applying volume {}={}.", setting.first, setting.second);
    (*spot)->set_db(setting.second);
    return true;
}

void configuration::apply_startup_settings(scarlett::idevice & dev) const
{
    auto settings {get_settings(dev.get_device_id())};
    DEBUG("Applying...");
    for (auto const & setting : settings)
        if (!apply_volume_setting(setting, dev.get_volumes()))
            if (!apply_enum_setting(setting, dev.get_enums()))
                ERROR("Unable to apply setting {{}, {}}.", setting.first, setting.second);
    DEBUG("Finished applying.");
}

std::pair<std::string, std::string> split_key_value(std::string const & line)
{
    auto spot {line.find('=')};
    if (spot == std::string::npos)
        return {};
    return {line.substr(0, spot), line.substr(spot + 1)};
}

std::filesystem::path configuration::get_device_config_file(std::string const & dev, bool create) const
{
    std::filesystem::path p {config_file};
    if (!std::filesystem::exists(p.parent_path()))
    {
        DEBUG("Configuration file doesn't exist...");
        if (!create)
        {
            DEBUG("Not creating.");
            return "";
        }
        if (!std::filesystem::create_directories(p.parent_path()))
        {
            ERROR("Error creating directory for configuration file, {}.", config_file);
            return "";
        }
        DEBUG("Created configuration file");
    }
    if (std::filesystem::exists(p))
    {
        std::ifstream config_reader {p};
        if (config_reader.fail())
            return "";
        std::string line;
        unsigned line_num {0};
        while (std::getline(config_reader, line))
        {
            if (line[0] == '#')
                continue;
            auto kvp {split_key_value(line)};
            if (!kvp.first.size())
            {
                DEBUG("Reading configuration file, {}. Line {} is not valid ({}). Ignoring.",
                    config_file, line_num, line);
                ++line_num;
                continue;
            }
            ++line_num;
            if (kvp.first != dev)
                continue;
            return kvp.second;
        }
    }
    INFO("Did not find configuration for device, {}.", dev);
    if (!create)
        return "";
    std::ofstream config_writer {p, std::ios_base::app};
    if (config_writer.fail())
    {
        ERROR("Error creating configuration file, {}.", config_file);
        return "";
    }
    auto dev_file {p};
    dev_file.replace_filename({dev});
    config_writer << dev << '=' << dev_file.native() << '\n';
    if (!std::filesystem::exists(dev_file))
        std::ofstream df {dev_file};
    return dev_file;
}

configuration::settings_t configuration::get_settings(std::string const & device) const
{
    DEBUG("Getting settings");
    auto device_conf {get_device_config_file(device)};
    if (!std::filesystem::exists(device_conf))
        return {};
    std::ifstream f {device_conf};
    std::string line;
    settings_t ret;
    unsigned line_num {0};
    while (std::getline(f, line))
    {
        auto kvp {split_key_value(line)};
        if (!kvp.first.size())
        {
            DEBUG("Reading device config, {}. Line {} is not valid ({}). Ignoring.",
                device_conf.native(), line_num, line);
            ++line_num;
            continue;
        }
        ++line_num;
        long val {-11111111};
        if (!kvp.second.size())
            /* Don't set a value if none was specified. */
            continue;
        else
        {
            try
            {
                val = stol(kvp.second);
            }
            catch (...)
            {
                ERROR("Reading line {} of device config file, {}. Line is {}.", line_num, device_conf.native(), line);
                throw;
            }
        }
        ret.push_back({kvp.first, val});
    }
    return ret;
}

void configuration::save_configuration(scarlett::idevice const & dev) const
{
    auto id {dev.get_device_id()};
    auto device_conf {get_device_config_file(id, true)};
    INFO("Saving configuration for device, {} (file {}).", id, device_conf.native());
    if (device_conf.empty())
    {
        ERROR("Could not get device configuration for {}.", id);
        return;
    }
    std::ofstream f {device_conf};
    for (auto const & vol : dev.get_volumes())
        f << vol->get_name() << "=" << vol->get_db() << '\n';
    for (auto const & e : dev.get_enums())
        f << e->get_name() << "=" << e->get_selection() << '\n';
}
