#include "stdafx.hpp"

#include "logging.hpp"
#include <scarlett/scarlett.hpp>
#include "mix_panel.hpp"

mix_panel::mix_panel(wxWindow * parent, std::string const & name)
    : wxWindow {parent, -1}
    , input_name {name}
{
    init();
}

mix_panel::mix_panel(wxWindow * parent, int min_vol, int max_vol, int current, std::string const & name)
    : wxWindow {parent, -1}
    , input_name {name}
    , volume {new wxSlider {this, wxID_ANY, current, min_vol, max_vol, wxDefaultPosition, wxDefaultSize, wxSL_VERTICAL | wxSL_INVERSE}}
    , vol_txt {new wxStaticText {this, wxID_ANY, volume_to_string(current)}}
{
    init();
    auto func {[this](auto & a) { user_changed_volume(a.GetPosition()); }};
    volume->Bind(wxEVT_SCROLL_THUMBTRACK, func);
    volume->Bind(wxEVT_SCROLL_CHANGED, func);
}

bool mix_panel::has_volume(void) const
{
    return volume != nullptr;
}

void mix_panel::init(void)
{
    SetAutoLayout(true);
    input_txt = new wxStaticText {this, -1, input_name};
    remove = new wxButton {this, wxID_ANY, "-"};
    auto font {remove->GetFont()};
    font.SetPointSize(font.GetPointSize() * 2);
    remove->SetFont(font);
    remove->SetToolTip("Remove input from mix");
    remove->Bind(wxEVT_BUTTON, [this](auto const &) {if (remove_me) remove_me(); });
    Bind(wxEVT_PAINT, [this](auto & e) { paint(e); });
}

void mix_panel::center(wxWindow * win) const
{
    auto my_size {GetSize()};
    auto target_size {win->GetSize()};
    auto margin = my_size.GetWidth() - target_size.GetWidth();
    win->SetSize(margin / 2, wxDefaultCoord, wxDefaultCoord, wxDefaultCoord, wxSIZE_USE_EXISTING);
}

bool mix_panel::Layout(void)
{
    int const padding {7};
    remove->SetSize(0, padding, 75, 35);
    input_txt->Move(0, remove->GetRect().GetBottom());
    if (volume)
    {
        auto my_size {GetSize()};
        auto next_top {input_txt->GetRect().GetBottom()};
        auto vol_height {my_size.GetHeight() - next_top - padding - vol_txt->GetSize().GetHeight()};
        std::clamp(vol_height, 0, 200);
        volume->SetSize(0, next_top, 75, vol_height);
        next_top += vol_height;
        vol_txt->Move(0, next_top);
        center(volume);
        center(vol_txt);
        DEBUG("Setting volume height to %d; min is %d, %d.", vol_height, volume->GetMinWidth(), GetMinHeight());
    }
    center(remove);
    center(input_txt);
    return true;
}

void mix_panel::paint(wxPaintEvent &)
{
    wxPaintDC dc {this};
    auto size {GetSize()};
    dc.SetPen(wxPen {wxColour {255, 150, 15}});
    dc.DrawRectangle(0, 0, size.GetWidth() - 1, size.GetHeight() - 1);
}

void mix_panel::fire_changed(void) const
{
    if (!volume_change_handler)
        return;
    volume_change_handler(volume->GetValue());
}

/*
 * When this is called after a wxSlider event, we need to force so that the event gets fired.
 */
void mix_panel::user_changed_volume(int new_val)
{
    vol_txt->SetLabel(volume_to_string(new_val));
    DEBUG("set volume text to %s.", volume_to_string(new_val).c_str());
    fire_changed();
}

std::string const & mix_panel::get_input_name(void) const
{
    return input_name;
}

void mix_panel::set_volume(int new_vol)
{
    if (volume == nullptr && new_vol != scarlett::mixer_wrapper::no_volume)
        ERROR("Attempt to set volume on NULL volume component! (input %s.)", input_name.c_str());
    else if (volume->GetValue() != new_vol)
        volume->SetValue(new_vol);
}

void mix_panel::register_volume_change(volume_change_handler_t const & h)
{
    volume_change_handler = h;
}

void mix_panel::register_remove_requested(void_func_t const & h)
{
    remove_me = h;
}
