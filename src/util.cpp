#include "stdafx.hpp"

#include "logging.hpp"
#include "util.hpp"

void callback(wxApp * app, std::function<void(void)> const & func)
{
    /* For thread safety */
    auto top_window {app->GetTopWindow()};
    if (!top_window)
        return;
    top_window->GetEventHandler()->CallAfter(func);
}

std::string volume_to_string(long val)
{
    return wxString::Format("%.2f", static_cast<double>(val) / 100).ToStdString();
}
